﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
    

public class FixedSizeGrid<T>
{
    public int Rows { get; private set; }
    public int Columns { get; private set; }

    private T[] _array;
    public T[] Array
    {
        get
        {
            return _array;
        }

        set
        {
            if (value.Length != Rows * Columns)
            {
                throw new Exception("Array Length must be Rows * Columns(" + Rows * Columns + ") in length. Array size is: " + value.Length);
            }
            _array = value;
        }
    }

    public FixedSizeGrid(int rows, int columns)
    {
         Rows = rows;
         Columns = columns;
        _array = new T[Rows * Columns];
    }

    public FixedSizeGrid(int rows, int columns, T[] array)
    {
        Rows = rows;
        Columns = columns;
        Array = array;
    }

    public T this[int row, int column]
    {
        get
        {
            PositionCheck(row, column);
            var arrayPosition = (row * Columns) + column;
            return _array[arrayPosition];
        }

        set
        {
            PositionCheck(row, column);
            var arrayPosition = (row * Columns) + column;
            _array[arrayPosition] = value;
        }
    }

    private void PositionCheck(int row, int column)
    {
        if (!IsValidRow(row))
        {
            throw new Exception("row Out Of Range. row must be between 0 - " + (Rows - 1) + " row: " + row);
        }
        if (!IsValidColumn(column))
        {
            throw new Exception("column Out Of Range. column must be between 0 - " + (Columns - 1) + " column: " + column);
        }
    }

    public bool IsValidPosition(int row, int column)
    {
        return IsValidRow(row) && IsValidColumn(column);
    }

    public bool IsValidRow(int row)
    {
        return row >= 0 && row < Rows;
    }

    public bool IsValidColumn(int column)
    {
        return column >= 0 && column < Columns;
    }

    public void Set(T[] array)
    {
        if (array.Length != Rows * Columns)
        {
            throw new Exception("Array Length must be Rows * Columns(" + Rows * Columns +") in length. Array size is: " + array.Length);
        }
        _array = array;
    }

    public void SetRow(int rowIndex, T[] array)
    {
        if (array.Length != Columns)
        {
            throw new Exception("Array Length must be the size of a Row (" + Columns + ") in length. Array size is: " + array.Length);
        }
        if (!IsValidRow(rowIndex))
        {
            throw new Exception("rowIndex Out Of Range. rowIndex must be between 0 - " + (Rows - 1));
        }
        for (int i = 0; i < Rows; i++)
        {
            this[rowIndex, i] = array[i];
        }
    }

    public void SetColumn(int columnIndex, T[] array)
    {
        if (array.Length != Rows)
        {
            throw new Exception("Array Length must be the size of a Column (" + Rows + ") in length. Array size is: " + array.Length);
        }
        if (!IsValidColumn(columnIndex))
        {
            throw new Exception("columnIndex Out Of Range. columnIndex must be between 0 - " + (Columns-1));
        }
        for (int i = 0; i < Columns; i++)
        {
            this[i, columnIndex] = array[i];
        }
    }

    public T[] GetRow(int rowIndex)
    {
        var items = new T[Columns];
        for (int i = 0; i < Columns; i++)
        {
            items[i] = this[rowIndex, i];
        }
        return items;
    }

    public T[] GetColumn(int columnIndex)
    {
        var items = new T[Rows];
        for (int i = 0; i < Columns; i++)
        {
            items[i] = this[i, columnIndex];
        }
        return items;
    }

    public override string ToString()
    {
        return "[" + Rows + "," + Columns + "] " + base.ToString(); 
    }
}
